# mailtrain-form-post-sidecar #

`mailtrain-form-post-sidecar` is a small webserver that exposes a `/subscribe` for frontends that want to add subscriptions to a [mailtrain](https://mailtrain.org/) instance.

This project was made to go along with my [blog post on setting up mailtrain on Kubernetes](https://vadosware.io/post/setting-up-mailtrain-on-k8s/), since mailtrain [doesn't have a simple way to POST forms](https://github.com/Mailtrain-org/mailtrain/issues/101) (as of February 2020).

# How it works #

Given a valid API key for a `mailtrain` instance, `mailtrain-form-post-sidecar` and exposes an endpoint (`/subscribe`) that:

- Takes an email address to add as a subscriber
- Takes optional fields to add to the subscription
- Takes optional segments to add to the subscription

As it is expected to work via [HTTP form POST](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST) (which does not require [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing) setup), you will need to either create a form or build the appropriate AJAx request to send to the server.

# Usage #

## Docker ##

Run the [docker image associated with this repository](https://gitlab.com/mrman/mailtrain-form-post-sidecar/container_registry) next to your `mailtrain` instance with the appropriate configuration.

```shell
docker run -d --rm \
    -p 5000:5000 \
    -e MAILTRAIN_API_KEY=<generated mailtrain API key> \
    -e MAILTRAIN_API_URL=http://localhost:3000/api \
    -e MAILTRAIN_LIST_ID=<id of the list you want to use> \
    -e MAILTRAIN_REQUIRE_CONFIRMATION=true \
    --name mailtrain-form-post-sidecar \
    registry.gitlab.com/mrman/mailtrain-form-post-sidecar:0.1.0
```
(AKA `make docker-container`, you might want to a [`direnv`](https://direnv.net)-powered `.envrc` before running the `docker-container` `make` target)

By default `mailtrain-form-post-sidecar` listens on port 5000, see [configuration](#configuration) for more details.

## Binary ##

0. Download & build the source code
1. Set up required ENV variables
1. Run `mailtrain-form-post-sidecar` next to a live `mailtrain` with required ENV variables set
2. Expose `mailtrain-form-post-sidecar` next to the `mailtrain` instance under whatever URL you'd like (ex. `yourdomain.tld/subscribe`)
3. Create a form and set it's `action` to the externally accessible URL from step #3

## Kubernetes ##

You can test `mailtrain-form-post-sidecar` locally by port forwarding the ports of your `mailtrain` instance locally and running like so:

```shell
$ kubectl port-forward mailtrain-<id> 3004 -n <namespace> &
$ kubectl port-forward mailtrain-<id> 3000 -n <namespace> &
```

After setting up the port forwarding you can run `mailtrain-form-post-sidecar`:

```shell
$ export MAILTRAIN_API_KEY=<generated mailtrain API key>
$ export MAILTRAIN_API_URL=http://localhost:3000/api
$ export MAILTRAIN_LIST_ID=<id of the list you want to use>
$ export MAILTRAIN_REQUIRE_CONFIRMATION=true
$ mailtrain-form-post-sidecar
```

While `mailtrain-form-post-sidecar` is expected to sit behind an [TLS-terminating proxy](https://en.wikipedia.org/wiki/TLS_termination_proxy), it is not strictly required.

# Configuration #

`mailtrain-form-post-sidecar` receives configuration via ENV

| variable                         | default   | example                     | required? | description                                     |
|----------------------------------|-----------|-----------------------------|-----------|-------------------------------------------------|
| `HOST`                           | `0.0.0.0` | `127.0.0.1`                 | no        | Server Host                                     |
| `PORT`                           | `5000`    | `5000`                      | no        | Server Port                                     |
| `MAILTRAIN_API_KEY`              | N/A       |                             | yes       | The API key to use for mailtrain                |
| `MAILTRAIN_API_URL`              | N/A       | `http://localhost:3000/api` | yes       | The URL at which to reach the mailtrain API     |
| `MAILTRAIN_LIST_ID`              | N/A       | `xRd_oomB`                  | yes       | The ID of the Mailtrain list you want to add to |
| `MAILTRAIN_REQUIRE_CONFIRMATION` | `true`    | `true`                      | no        | Whether to require confirmation (via email)     |
| `MAILTRAIN_FORCE_SUBSCRIBE`      | `false`   | `true`                      | no        | Whether to force subscriptions                  |

# Example #

## Proxy Configuration ##

Here's an example of what the configuration would look like for the reverse proxy [NGINX](https://nginx.org):

```
user nobody nogroup;
worker_processes auto;          # auto-detect number of logical CPU cores

events {
    worker_connections 512;       # set the max number of simultaneous connections (per worker process)
}

http {
    server {
        listen *:80;                # Listen for incoming connections from any interface on port 80
        include /etc/nginx/mime.types;
        root /usr/share/nginx/html; # serve static files from here

        ###########
        # Caching #
        ###########

        # Media: images, icons, video, audio, HTC
        location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|svgz|mp4|ogg|ogv|webm|htc)$ {
            expires 1M;
            access_log off;
            add_header Cache-Control "public";
        }

        # CSS and Javascript
        location ~* \.(?:css|js)$ {
            expires 1y;
            access_log off;
            add_header Cache-Control "public";
        }

        location ~ ^/mailing-list {
            proxy_pass http://localhost:5000/;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/local/nginx/html;
        }
    }
}
```

With this configuration, your site (assumed to be hosted at `/var/www/html`) is accessible at `/` (externally `yourdomain.tld/`), and `mailtrain-form-post-sidecar` is available at `/mailing-list` (externally `yourdomain.tld/mailing-list/subscribe` would be where you `POST` a `<form>` to).

## HTML Form integration ##

On your website, you might add a HTML snippet like the following to allow people to sign up to your `mailtrain`-managed mailing list:

```html
<form method="POST" action"/path/to/subscribe">
  <input type="email" placeholder="email@domain.com" name="email"/>
  <button type="submit">Subscribe</button>
</form>
```

When users submit the form, a `POST` request will be sent to `mailtrain-form-post-sidecar`.

## EmailCTA with `maille` ##

[`maille`](https://mrman.gitlab.io/maille/#components-email-cta) is a lightweight Mithril component framework that comes with a handy `EmailCTA` component that can be used for this purpose:

```html
<head>
<!-- ... some other html -->
    <link rel="stylesheet" href="/vendor/maille/maille.min.css">
    <link rel="stylesheet" href="/vendor/maille/mailing-list-cta.shared.min.css">
    <style>
     section#mailing-list-cta-container .maille.component.mailing-list-cta {
       margin-top: 2em;
       box-shadow: 1px 10px 10px #CCC;
       padding: 1em;
       width: 100%;
     }
    </style>
<!-- ... some other html -->
</head>

<body>
<!-- ... some other html -->

<section id="mailing-list-cta-container">
  <div id="mailing-list-cta"></div>
  <script src="/vendor/mithril/2.0.4/mithril.min.js"></script>
  <script src="/vendor/maille/mailing-list-cta.shared.min.js"></script>
  <script>
   var MailingListCTA = MAILLE_MAILING_LIST_CTA.default;

   // Render the component
   document.addEventListener("DOMContentLoaded", function() {
     m.render(
       document.querySelector("div#mailing-list-cta"),
       m(
         MailingListCTA,
         {
           brief: "Want to get an email whenever I add more posts like this one?",
           subscribeForm: {action: "/mailing-list/subscribe"}
         },
       ),
     );
   });
  </script>
</section>

<!-- ... some other html -->
</body>
```
