#[macro_use] extern crate tower_web;
#[macro_use] extern crate log;
#[macro_use] extern crate rust_embed;
extern crate ureq;
use serde::{Serialize, Deserialize};

use tower_web::ServiceBuilder;
use std::net::{SocketAddr};
use std::env;

#[derive(Clone, Debug)]
struct MailtrainFormPost {
    /// Address the server will run on
    server_addr: SocketAddr,
    /// API key to send to mailtrain
    mailtrain_api_key: String,
    /// URL at which mailtrain is hosted (ex. "https://mailtrain.yoursite.com", "localhost:3000")
    mailtrain_api_url: String,
    /// ID of the mailtrain mailing list (ex. "B16uvTzz")
    mailtrain_list_id: String,
    /// Whether to force the subscription or not
    mailtrain_force_subscribe: bool,
    /// Whether to require confirmation or not
    mailtrain_require_confirmation: bool,
}

#[derive(Extract, Clone, Debug, Serialize, Deserialize)]
struct SubscribeForm {
    email: String,
    #[serde(skip_serializing)]
    first_name: Option<String>,
    #[serde(skip_serializing)]
    last_name: Option<String>,
    #[serde(skip_serializing)]
    timezone: Option<String>,
    force_subscribe: Option<String>,
    require_confirmation: Option<String>,
}

#[derive(RustEmbed)]
#[folder = "public/"]
struct Asset;

impl_web! {
    impl MailtrainFormPost {
        /// Subscription endpoint
        #[post("/subscribe")]
        #[content_type("text/html")]
        fn subscribe(&self, mut body: SubscribeForm) -> Result<String, ()> {
            // Override the settings for forcing subscription/requiring confirmation
            // in the future maybe we'll allow this to come directly from the POST request
            body.force_subscribe = Some(String::from(if self.mailtrain_force_subscribe { "yes" } else { "" }));
            body.require_confirmation = Some(String::from(if self.mailtrain_require_confirmation { "yes" } else { "" }));

            // Pull the sub information
            let email = body.email.clone();
            let maybe_sub_info = serde_json::to_value(body);
            if let Err(_err) = maybe_sub_info {
                return Ok(String::from("Bad request, invalid JSON"));
            }
            let sub_info = maybe_sub_info.unwrap();

            // Build full URL for subscribe POSt
            let url = format!(
                "{}/subscribe/{}?access_token={}",
                self.mailtrain_api_url,
                self.mailtrain_list_id,
                self.mailtrain_api_key,
            );

            // Perform the request
            debug!("Sending sub -> {}", &sub_info);
            let resp = ureq::post(url.as_str()).send_json(sub_info);

            // If an error happened then return a generic error
            if !resp.ok() {
                match resp.into_string() {
                    Ok(body_str) => {
                        warn!("Mailtrain request failed -> {}", body_str);
                        warn!("Hint: are the incoming email address properly url encoded?");
                    }
                    Err(err) => warn!("Failed to convert response body for printing...: {}", err),
                }

                return get_asset_contents("subscription-failed.html");
            }
            debug!("Successfully subscribed email [{}]", email);

            // Retrieve the compiled-in asset
            return get_asset_contents("subscription-successful.html");
        }
    }
}

/// Get the contents of some asset
fn get_asset_contents(asset_path: &str) -> Result<String, ()> {
    // TODO: Enable loading local files instead of only compiled in assets

    // Retrieve the compiled-in asset
    let maybe_contents = Asset::get(asset_path);
    if maybe_contents.is_none() {
        error!("Unexpected failure retrieving asset subscription-successful.html");
        return Ok(String::from("An unexpected error occurred! Please contact support"));
    }
    let contents = maybe_contents.unwrap();

    // Parse the HTML from UTF8
    let maybe_parsed = std::str::from_utf8(contents.as_ref());
    if let Err(err) = maybe_parsed {
        error!("Failed to convert utf8 for page to String: {}", &err);
        return Ok(String::from("An unexpected error occurred! Please contact support"));
    }

    Ok(String::from(maybe_parsed.unwrap()))
}

// Server ENV config values
const ENV_KEY_PORT: &'static str = "PORT";
const DEFAULT_PORT: &'static str = "5000";
const ENV_KEY_HOST: &'static str = "HOST";
const DEFAULT_HOST: &'static str = "0.0.0.0";

// Mailtrain ENV config values
const ENV_KEY_MAILTRAIN_API_KEY: &'static str = "MAILTRAIN_API_KEY";
const ENV_KEY_MAILTRAIN_API_URL: &'static str = "MAILTRAIN_API_URL";
const ENV_KEY_MAILTRAIN_LIST_ID: &'static str = "MAILTRAIN_LIST_ID";
const ENV_KEY_MAILTRAIN_FORCE_SUBSCRIBE: &'static str = "MAILTRAIN_FORCE_SUBSCRIBE";
const ENV_KEY_MAILTRAIN_REQUIRE_CONFIRMATION: &'static str = "MAILTRAIN_REQUIRE_CONFIRMATION";

/// Get a value from ENV or panic
fn get_env_str_or_panic(desc: &str, key: &str) -> String {
    // Read in the value for the given key if present
    if let Err(_) = env::var(key) {
        error!("{} not provided (ENV var {})", desc, key);
        panic!(format!("{} not provided", desc));
    }
    let value = env::var(key).unwrap();

    value
}

/// Get a value that is intended to be a boolean from ENV
fn get_env_bool(key: &str) -> bool {
    match env::var(key) {
        Ok(v) => {
            match v.to_ascii_lowercase().as_str() {
                "true" => true,
                "yes" => true,
                _ => false,
            }
        }
        Err(_err) => false
    }
}

fn main() {
    env_logger::init();

    // Read in server variables from env
    let port = env::var(ENV_KEY_PORT).unwrap_or(String::from(DEFAULT_PORT));
    let host = env::var(ENV_KEY_HOST).unwrap_or(String::from(DEFAULT_HOST));
    let server_addr = format!("{}:{}", host, port).parse().expect("Invalid host/port combination");

    // Read in required mailtrain information from ENV
    let mailtrain_api_key = get_env_str_or_panic("Mailtrain API key", ENV_KEY_MAILTRAIN_API_KEY);

    let mailtrain_api_url = get_env_str_or_panic("Mailtrain server URL", ENV_KEY_MAILTRAIN_API_URL);
    info!("{: <30} | [{}]", ENV_KEY_MAILTRAIN_API_URL, mailtrain_api_url);

    let mailtrain_list_id = get_env_str_or_panic("Mailtrain list ID", ENV_KEY_MAILTRAIN_LIST_ID);
    info!("{: <30} | [{}]", ENV_KEY_MAILTRAIN_LIST_ID, mailtrain_list_id);

    let mailtrain_force_subscribe = get_env_bool(ENV_KEY_MAILTRAIN_FORCE_SUBSCRIBE);
    info!("{: <30} | [{}]", ENV_KEY_MAILTRAIN_FORCE_SUBSCRIBE, mailtrain_force_subscribe);

    let mailtrain_require_confirmation = get_env_bool(ENV_KEY_MAILTRAIN_REQUIRE_CONFIRMATION);
    info!("{: <30} | [{}]", ENV_KEY_MAILTRAIN_REQUIRE_CONFIRMATION, mailtrain_require_confirmation);

    info!("Serving on [{}]...", server_addr);
    ServiceBuilder::new()
        .resource(MailtrainFormPost {
            server_addr,
            mailtrain_api_key,
            mailtrain_api_url,
            mailtrain_list_id,
            mailtrain_force_subscribe,
            mailtrain_require_confirmation,
        })
        .run(&server_addr)
        .unwrap();
}
