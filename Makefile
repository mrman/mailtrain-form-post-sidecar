.PHONY: build clean clean-runtime run test test-e2e watch \
				check-tool-cargo check-tool-cargo-watch \
				docker-image docker-publish docker-registry-login get-version

all: build

VERSION=$(shell awk '/version\s+=\s+"([0-9|\.]+)"$$/{print $$3;exit;}' Cargo.toml)

CARGO := $(shell command -v cargo 2> /dev/null)
CARGO_WATCH := $(shell command -v cargo-watch 2> /dev/null)

IMAGE_NAME:=mailtrain-form-post-sidecar
REGISTRY_PATH=registry.gitlab.com/mrman
FQ_IMAGE_NAME=$(REGISTRY_PATH)/$(IMAGE_NAME):$(VERSION)

check-tool-cargo:
ifndef CARGO
	$(error "`cargo` is not available please install cargo (https://github.com/rust-lang/cargo/)")
endif

check-tool-cargo-watch:
ifndef CARGO_WATCH
	$(error "`cargo-watch` is not available please install cargo-watch (https://github.com/passcod/cargo-watch).\n'cargo install cargo-watch' should do it!")
endif

get-version:
	@echo -e ${VERSION}

dev-setup:
	cp .dev/git/hooks/pre-push .git/hooks && chmod +x .git/hooks/pre-push

clean: check-tool-cargo clean-runtime
	cargo clean

clean-runtime:
	rm -rf infra/runtime

build: check-tool-cargo
	cargo build

build-release: check-tool-cargo
	cargo build --release

run:
	cargo run

test:
	cargo test

test-e2e:
	cargo test -- --ignored

watch: check-tool-cargo check-tool-cargo-watch
	cargo watch

docker-image:
		docker build -f infra/docker/Dockerfile -t $(FQ_IMAGE_NAME) .

docker-registry-login:
		docker login registry.gitlab.com

docker-publish: docker-registry-login
		docker push $(FQ_IMAGE_NAME)

docker-container:
		docker run -d --rm \
		-e MAILTRAIN_API_KEY=$(MAILTRAIN_API_KEY) \
		-e MAILTRAIN_API_URL=$(MAILTRAIN_API_URL) \
		-e MAILTRAIN_LIST_ID=$(MAILTRAIN_LIST_ID) \
		-e MAILTRAIN_REQUIRE_CONFIRMATION=$(MAILTRAIN_REQUIRE_CONFIRMATION) \
		--name mailtrain-form-post-sidecar \
		-e RUST_LOG=$(RUST_LOG) \
		$(FQ_IMAGE_NAME)
